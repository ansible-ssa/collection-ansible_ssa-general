# Ansible Collection - ansible_ssa.general

Documentation for the collection.

This collection is automatically build and contains the following roles. Please read the respective role documentation for more details.

- automationhub_content
- automationhub_setup
- controller_content
- controller_setup
- epel
- insights_registration
- instance
- nsupdate
- package_downloader
- rhc_install
- rhsm_registration
